# Serbian translation for tracker-miner-chatlog.
# Courtesy of Prevod.org team (http://prevod.org/) -- 2016.
# Copyright (C) 2016 tracker-miner-chatlog's COPYRIGHT HOLDER
# This file is distributed under the same license as the tracker-miner-chatlog package.
# Мирослав Николић <miroslavnikolic@rocketmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: tracker-miner-chatlog master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-01-03 15:27+0000\n"
"PO-Revision-Date: 2016-01-03 23:01+0200\n"
"Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>\n"
"Language-Team: Serbian <gnom@prevod.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : "
"n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: ../data/org.freedesktop.Tracker1.Miner.ChatLog.service.in.h:1
#: ../data/tracker-miner-chatlog.desktop.in.in.h:1
msgid "Chat logs"
msgstr "Дневници ћаскања"

#: ../data/org.freedesktop.Tracker1.Miner.ChatLog.service.in.h:2
#: ../data/tracker-miner-chatlog.desktop.in.in.h:2
msgid "Chat logs miner"
msgstr "Копач дневника ћаскања"
